GSD Worklist Status Log Analyzer
--------------------------------
This handy, dandy notebook - errr I mean - tool allows user to see a visual representation
of action time durations of a worklist run.

-- How To Use --
This tool uses .NET 5.0. If there is an issue installing, please be sure to have .NET 5.0 runtime installed.
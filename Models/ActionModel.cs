﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDWorklistLogAnalyzerTool.Models
{
    public class ActionModel
    {
        public string ActionName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public double StartWell { get; set; }
        public double EndWell { get; set; }
    }
}

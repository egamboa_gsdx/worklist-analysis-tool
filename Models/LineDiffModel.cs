﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDWorklistLogAnalyzerTool.Models
{
    public class LineDiffModel
    {
        public int Index { get; set; }
        public string LineDiff { get; set; }
    }
}

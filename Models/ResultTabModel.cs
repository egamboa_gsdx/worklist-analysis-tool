﻿using GalaSoft.MvvmLight.Command;
using LiveCharts;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Excel = Microsoft.Office.Interop.Excel;

namespace GSDWorklistLogAnalyzerTool.Models
{
    [AddINotifyPropertyChangedInterface]
    public class ResultTabModel
    {
        private string _filename;
        public ResultTabModel()
        {
            TestNames = new ObservableCollection<string>();
            Tests = new ObservableCollection<TestResultModel>();
            StatusLogs = new ObservableCollection<StatusLogModel>()
            {
                new StatusLogModel()
                {
                    Name = string.Empty,
                    WorklistName = string.Empty
                }
            };
            CompareList = new ObservableCollection<StatusLogModel>();
            SeriesCollection = new SeriesCollection();
            CompareCollection = new SeriesCollection();
            CompareTestLabels = new ObservableCollection<string>();
            TestResults = new ObservableCollection<TestResultModel>();
            ActionTimes = new ObservableCollection<ActionTimeModel>();
            ActionTimesBarGraph = new ObservableCollection<ActionTimeModel>();
            CompareBtnVisible = Visibility.Hidden;
        }

        public override bool Equals(object obj)
        {
            if (obj is ResultTabModel tb)
            {
                return tb.Name == Name;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        public bool CanExport()
        {
            return CompareCollection.Any();
        }

        public string Header { get; set; }
        public string Name { get; set; }
        public string FileName { get { return _filename; } set { _filename = value; } }
        public string WorklistName { get; set; }
        public string OperatorName { get; set; }
        public string NumberOfTestsRun { get; set; }
        public string NumberOfSamplesLoaded { get; set; }
        public TimeSpan TotalRuntime { get; set; }
        public ObservableCollection<string> TestNames { get; set; }
        public ObservableCollection<TestResultModel> Tests { get; set; }
        public ObservableCollection<TestResultModel> TestResults { get; set; }
        public ObservableCollection<StatusLogModel> StatusLogs { get; set; }
        public ObservableCollection<StatusLogModel> CompareList { get; set; }
        public ObservableCollection<ActionTimeModel> ActionTimes { get; set; }
        public ObservableCollection<ActionTimeModel> ActionTimesBarGraph { get; set; }
        public ObservableCollection<TimeSpan> IncubationTime { get; set; }
        public List<ChartValues<double>> ActionResults { get; set; }
        public ChartValues<double> IncubationResults { get; set; }
        public string Wash1 { get; set; }
        public string Wash2 { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationVersion { get; set; }
        public string ComputerName { get; set; }
        public string InstrumentName { get; set; }
        public DateTime DateRan { get; set; }
        public string DetailTextLeft { get; set; }
        public string DetailTextRight { get; set; }
        public string DetailTextMid { get; set; }
        public string DetailHeader1 { get; set; }
        public string DetailHeader2 { get; set; }
        public int TabCount { get; set; }
        public Func<double, string> Formatter { get; set; } = val => TimeSpan.FromSeconds(Math.Abs(val)).ToString("c");
        public SeriesCollection SeriesCollection { get; set; }
        public SeriesCollection CompareCollection { get; set; }
        public ObservableCollection<string> CompareTestLabels { get; set; }
        public bool ExportEnabled { get; set; } = false;
        public bool CalculateDriftEnabled { get; set; }
        public Visibility CompareBtnVisible { get; set; }
        public StatusLogModel SelectedItem
        {
            get { return _selectedItem; } 
            set
            {
                if (_selectedItem == value) return;
                _selectedItem = value;
            }
        }
        private StatusLogModel _selectedItem;

    }
}

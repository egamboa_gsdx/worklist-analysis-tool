﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDWorklistLogAnalyzerTool.Models
{
    public class StatusLogModel
    {
        public StatusLogModel()
        {
            TestResults = new ObservableCollection<TestResultModel>();
        }
        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            if(obj is StatusLogModel sm)
            {
                return WorklistName == sm.WorklistName;
            }
            return false;
        }
        public string WorklistName { get; set; }
        public string Name { get; set; }
        public ObservableCollection<TestResultModel> TestResults { get; set; }
    }
    
}

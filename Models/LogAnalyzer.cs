﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using Microsoft.Office.Interop.Excel;
using PropertyChanged;
using System.Collections.ObjectModel;
using GSDWorklistLogAnalyzerTool.Enums;
using GSD.TestLab.DataCollection;
using GSD.TestLab.DataCollection.Enums;

namespace GSDWorklistLogAnalyzerTool.Models
{
    [AddINotifyPropertyChangedInterface]
    public class LogAnalyzer
    {
        private static string _fileLog;
        private Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
        private DataCollection _dataCollection;
        public LogAnalyzer(string s, ResultTabModel resultTab)
        {
            _fileLog = s;
            _dataCollection = new DataCollection();
            TestNames = new List<string>();
            Tests = new ObservableCollection<TestResultModel>();
            TestResults = new ObservableCollection<TestResultModel>();
            StatusLogs = new StatusLogModel();
            ActionTimes = new ObservableCollection<ActionTimeModel>();
            ParseLog(resultTab);
        }

        protected void ParseLog(ResultTabModel resultTab)
        {
            List<TestResultModel> testResults = new List<TestResultModel>();
            string[] analysis = File.ReadAllLines(_fileLog);
            resultTab.DateRan = File.GetLastWriteTime(_fileLog);

            StatusLogs.WorklistName = resultTab.WorklistName = DataCollection.GetWorklistName(analysis);
            resultTab.OperatorName = DataCollection.GetWorklistOperator(analysis);
            resultTab.Wash1 = DataCollection.GetWashBottle1(analysis);
            resultTab.Wash2 = DataCollection.GetWashBottle2(analysis);
            resultTab.ApplicationName = DataCollection.GetApplicationname(analysis);
            resultTab.ApplicationVersion = DataCollection.GetApplicationVersion(analysis);
            resultTab.ComputerName = DataCollection.GetComputerName(analysis);
            resultTab.InstrumentName = DataCollection.GetInstrumentName(analysis);
            resultTab.NumberOfSamplesLoaded = DataCollection.GetNumberOfSamplesLoaded(analysis);
            resultTab.NumberOfTestsRun = DataCollection.GetNumberOfTestRuns(analysis);
            var testNamesList = DataCollection.GetTestNames(analysis, int.Parse(resultTab.NumberOfTestsRun));
            foreach (var testname in testNamesList)
            {
                resultTab.TestNames.Add(testname);
                resultTab.Tests.Add(new TestResultModel() { Name = testname });
            }

            var totalruntimestring = analysis.SkipWhile(x => x != "----Worklist Actions----").Skip(1).TakeWhile(x => x != string.Empty).ToList();
            resultTab.TotalRuntime = _dataCollection.GetTotalWorklistRunTime(totalruntimestring);

            for (int i = 0; i < int.Parse(resultTab.NumberOfTestsRun); i++)
            {
                TestResultModel tr = new TestResultModel();
                tr.Name = $"T{i + 1}";
                var t = analysis.SkipWhile(x => x != tr.Name + " Runtime Results:").Skip(1).TakeWhile(x => !x.Contains("Incubation")).ToList();
                var t1 = analysis.SkipWhile(x => x != tr.Name + " Runtime Results:").Skip(1).TakeWhile(x => !x.Contains("Runtime Results")).ToList();
                tr.IncubationTime =  t1.Count != 0 ? RuntimeResultsDataCollection.GetRuntimeIncubationResults(t1) :
                    _dataCollection.GetActionTime(totalruntimestring, WorklistActionType.Incubation);
                tr.ActionTimes = GetActionTimeCollection(tr.Name, t);

                testResults.Add(tr);
            }

            foreach (var tr in testResults)
            {
                var sb = new List<string>();
                TimeSpan actionTime = TimeSpan.Zero;
                foreach (var runline in totalruntimestring)
                {
                    if (runline.Contains(tr.Name))
                    {
                        sb.Add(runline);
                    }
                }
                // get actions if Runtime Results are unavailable
                if (!tr.ActionTimes.Any())
                {
                    // get fluidtransfer actions
                    var fluids = _dataCollection.GetFluidTransferActionFromWorklist(totalruntimestring, tr.Name);
                    if (fluids.Any())
                    {
                        foreach (var fluid in fluids)
                        {
                            tr.ActionTimes.Add(new ActionTimeModel()
                            {
                                TestRunName = tr.Name,
                                ActionName = fluid.Item1,
                                ActionTime = fluid.Item2
                            });
                        }
                    }

                    var multishots = _dataCollection.GetMultiShotActionTimeFromWorklist(totalruntimestring, tr.Name);
                    if (multishots.Any())
                    {
                        foreach (var multishot in multishots)
                        {
                            tr.ActionTimes.Add(new ActionTimeModel()
                            {
                                TestRunName = tr.Name,
                                ActionName = multishot.Item1,
                                ActionTime = multishot.Item2
                            });
                        }
                    }

                    // get readwell actions
                    var readwells = _dataCollection.GetActionTime(sb, WorklistActionType.ReadWell);
                    if (readwells != TimeSpan.Zero)
                    {
                        tr.ActionTimes.Add(new ActionTimeModel()
                        {
                            TestRunName = tr.Name,
                            ActionName = "ReadWellAction; ReadWells",
                            ActionTime = readwells
                        });
                    }

                    // get WashWell actions
                    var washwells = _dataCollection.GetActionTime(sb, WorklistActionType.WashWell);
                    if (washwells != TimeSpan.Zero)
                    {
                        tr.ActionTimes.Add(new ActionTimeModel()
                        {
                            TestRunName = tr.Name,
                            ActionName = "WashWellAction; WashWells",
                            ActionTime = washwells
                        });
                    }
                }
                

                // Get Shaker action times
                tr.ShakerActionTime = _dataCollection.GetActionTime(sb, WorklistActionType.Shaker);
                if (tr.ShakerActionTime != TimeSpan.Zero)
                {
                    tr.ActionTimes.Add(new ActionTimeModel()
                    {
                        TestRunName = tr.Name,
                        ActionName = "ShakerAction",
                        ActionTime = tr.ShakerActionTime
                    });
                }
                

                // Get Heat action times
                tr.HeatActionTime = _dataCollection.GetActionTime(sb, WorklistActionType.Heat);
                if (tr.HeatActionTime != TimeSpan.Zero)
                {
                    tr.ActionTimes.Add(new ActionTimeModel()
                    {
                        TestRunName = tr.Name,
                        ActionName = "HeaterAction;",
                        ActionTime = tr.HeatActionTime.Duration()
                    });
                }

                // get probewashactions
                var probewash = _dataCollection.GetActionTime(sb, WorklistActionType.ProbeWash);
                if (probewash != TimeSpan.Zero)
                {
                    tr.ActionTimes.Add(new ActionTimeModel()
                    {
                        TestRunName = tr.Name,
                        ActionName = "ProbeWashAction",
                        ActionTime = probewash
                    });
                }

                // Get CaptureWellImageAction
                actionTime = _dataCollection.GetActionTime(sb, WorklistActionType.CaptureWellImage);
                if (actionTime != TimeSpan.Zero)
                {
                    tr.ActionTimes.Add(new ActionTimeModel()
                    {
                        TestRunName = tr.Name,
                        ActionName = "CaptureWellImageAction",
                        ActionTime = actionTime
                    });
                }

                // Get NeedleDecontaminateAction
                var needledecon = _dataCollection.GetActionTime(sb, WorklistActionType.NeedleDecon);
                if (needledecon != TimeSpan.Zero)
                {
                    tr.ActionTimes.Add(new ActionTimeModel()
                    {
                        TestRunName = tr.Name,
                        ActionName = "NeedleDecontaminateAction",
                        ActionTime = needledecon
                    });
                }

                
                ActionTimes = tr.ActionTimes;
                TestResults.Add(tr);
                StatusLogs.TestResults.Add(tr);
                if (Tests.Where(x => x.Name.Contains(tr.Name)).Any())
                {
                    var test = Tests.Where(x => x.Name.Contains(tr.Name)).FirstOrDefault();
                    test.ActionTimes = tr.ActionTimes;
                    test.IncubationTime = tr.IncubationTime;
                }
            }

            resultTab.TestResults = new ObservableCollection<TestResultModel>(TestResults.OrderBy(x => x.Name));

        }
      
        private bool IsExcelInstalled()
        {
            //if(xlApp == null)
            //{
            //    MessageBox.Show("Excel is not properly installed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return;
            //}
            return xlApp == null;
        }

        private  ObservableCollection<ActionTimeModel> GetActionTimeCollection(string testname, List<string> actionlines)
        {
            var actiontimes = new ObservableCollection<ActionTimeModel>();

            var runtimeResults = RuntimeResultsDataCollection.GetRuntimeResults(actionlines);
            foreach (var runtimeresult in runtimeResults)
            {
                actiontimes.Add(new ActionTimeModel()
                {
                    TestRunName = testname,
                    ActionName = runtimeresult.Item1,
                    ActionTime = runtimeresult.Item2
                }); ;
            }

            return actiontimes;
        }

        public ObservableCollection<TestResultModel> TestResults { get; private set; } 
        public StatusLogModel StatusLogs { get; private set; }
        public List<string> TestNames { get; private set; }
        public ObservableCollection<ActionTimeModel> ActionTimes { get; private set; }
        public ObservableCollection<TestResultModel> Tests { get; set; }
    }
}

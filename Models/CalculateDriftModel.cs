﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Excel = Microsoft.Office.Interop.Excel;

namespace GSDWorklistLogAnalyzerTool.Models
{
    public class CalculateDriftModel
    {
        private string _filename;
        private string _worklistname;

        public CalculateDriftModel(string filename)
        {
            _filename = filename;
            StreamReader streamReader = new StreamReader(filename);
            string line;
            while ((line = streamReader.ReadLine()) != null)
            {
                if (line.Contains("Worklist Name:")) _worklistname = line.Substring(line.IndexOf(": ") + 1);
            }
        }

        public void CalculateDrift()
        {
            string[] analysis = File.ReadAllLines(_filename);
            var t = analysis.SkipWhile(x => x != "----Worklist Actions----").Skip(1).TakeWhile(x => x != "----Worklist Runtime Summary----").ToList();
            List<DriftModel> dms = new List<DriftModel>();
            // Get List of Test Name(s)
            var testnames = analysis.SkipWhile(x => !(x.Contains("Number of Tests to run"))).Skip(1).TakeWhile(x => !(x.Contains("Wash Bottle"))).ToList();
            foreach(var testname in testnames)
            {
                dms.Add(new DriftModel()
                {
                    TestName = testname.Substring(testname.IndexOf("  ") + 2)
                });
            }
            List<string> actionblocks = new List<string>();
            dms = new List<DriftModel>(dms.OrderBy(x => x.TestName));
            foreach (var line in t)
            {
                if (line.Contains("D:MTP") || 
                    line.Contains("WashWellAction") || 
                    line.Contains("Finished test saved to database") || 
                    (line.Contains("ReadWellAction") && line.Contains("Read Well,")))
                {
                    actionblocks.Add(line);
                }
            }

            // get action time duration from first well to last well
            foreach (var dm in dms)
            {
                double lastwell = 0;
                double firstwell = 0;
                double well;
                int index = 1;
                string starttime = string.Empty;
                string endtime = string.Empty;

                foreach (var actionblock in actionblocks)
                {
                    
                    if (actionblock.Contains(dm.TestName.Substring(0, dm.TestName.IndexOf(":"))))
                    {
                        var wellstring = actionblock.Substring(actionblock.LastIndexOf("#") + 1);
                        string fuck = "0";
                        if (actionblock.Contains("D:MTP"))
                        {
                            fuck = wellstring.Substring(wellstring.IndexOf(".") + 1, wellstring.IndexOf(":") - 2);
                        }
                        else if (actionblock.Contains("WashWellAction"))
                        {
                            fuck = wellstring.Substring(wellstring.IndexOf(".") + 1, wellstring.IndexOf(",") - 2);
                        }
                        else if (actionblock.Contains("ReadWellAction") && actionblock.Contains("Read Well"))
                        {
                            fuck = wellstring.Substring(wellstring.IndexOf(".") + 1, wellstring.IndexOf(",") - 2);
                        }
                        else
                        {
                            fuck = double.NaN.ToString();
                        }

                        
                        well = Convert.ToDouble(fuck);
                        
                        if (lastwell == 0 && firstwell == 0)
                        {
                            lastwell = well;
                            firstwell = well;
                            starttime = actionblock.Substring(0, actionblock.IndexOf(";"));
                            endtime = actionblock.Substring(0, actionblock.IndexOf(";"));
                        }
                        else
                        {
                            if (lastwell < well)
                            {
                                lastwell = well;
                                endtime = actionblock.Substring(0, actionblock.IndexOf(";"));
                            }
                            else
                            {

                                dm.Actions.Add(new ActionModel()
                                {
                                    ActionName = string.Format("PoOp #{0}",index),
                                    StartTime = starttime,
                                    EndTime = endtime,
                                    StartWell = firstwell,
                                    EndWell = lastwell
                                });

                                // MessageBox.Show(string.Format("Name: POOP #{0} \n Start Time: {1} \n End Time: {2}", index, starttime, endtime));
                                firstwell = well;
                                starttime = actionblock.Substring(0, actionblock.IndexOf(";"));
                                lastwell = well;
                                endtime = actionblock.Substring(0, actionblock.IndexOf(";"));
                                index++;
                                if (actionblock.Contains("Finished test saved to database"))
                                {
                                    index = 0;
                                }
                            }
                        }
                        
                    }
                }
            }
            ExcelExport(dms);
        }

        private void ExcelExport(List<DriftModel> drifts)
        {
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlwb = null;
            Excel.Worksheet xlws = null;
            Excel.Range er = null;
            object misvalue = System.Reflection.Missing.Value;

            if (xlApp == null)
            {
                MessageBox.Show("Excel is not properly installed on this computer! Sucka!", "Excel Not Installed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                xlwb = xlApp.Workbooks.Add(misvalue);
                xlws = (Excel.Worksheet)xlwb.Worksheets.get_Item(1);

                // Add Data
                // Headers
                er = xlws.get_Range("A1", "I3");
                er.Font.Bold = true;
                er.EntireRow.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                er.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green);
                er.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);

                //Excel.FormatCondition format = xlws.Rows.FormatConditions.Add(Excel.XlFormatConditionType.xlExpression,
                //    Excel.XlFormatConditionOperator.xlEqual,
                //    "=MOD(ROW(),2) = 0");
                //format.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.PaleVioletRed);

                er = xlws.get_Range("A:J", misvalue);
                er.EntireColumn.ColumnWidth = 20;

                xlws.Range[xlws.Cells[1, 1], xlws.Cells[1, 9]].Merge();
                xlws.Cells[1, 1] = _worklistname;
                
                xlws.Range[xlws.Cells[2, 7], xlws.Cells[2, 8]].Merge();
                xlws.Cells[2, 7] = "Time Between";
                er = xlws.get_Range("G1", misvalue);
                er.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                xlws.Cells[3, 1] = "Test Name";
                xlws.Cells[3, 2] = "Action";
                xlws.Cells[2, 3] = "First Well Time";
                xlws.Cells[3, 3] = "(H:mm:ss)";
                xlws.Cells[2, 4] = "Last Well Time";
                xlws.Cells[3, 4] = "(H:mm:ss)";
                xlws.Cells[2, 5] = "Time Per Plate";
                xlws.Cells[3, 5] = "(H:mm:ss)";
                xlws.Cells[2, 6] = "Time per Well";
                xlws.Cells[3, 6] = "(H:mm:ss)";
                xlws.Cells[3, 7] = "First Well (H:mm:ss)";
                xlws.Cells[3, 8] = "Last Well (H:mm:ss)";
                xlws.Cells[3, 9] = "Drift (H:mm:ss)";

                var currenttest = drifts[0].TestName;
                xlws.Cells[4, 1] = currenttest;
                int cellindex = 0;

                for (int i = 0; i < drifts.Count; i++)
                {
                    for (int j = 0; j < drifts[i].Actions.Count; j++)
                    {
                        if (currenttest != drifts[i].TestName)
                        {
                            currenttest = drifts[i].TestName;
                            xlws.Cells[4 + cellindex, 1] = currenttest;
                        }
                        xlws.Cells[4 + cellindex, 2] = drifts[i].Actions[j].ActionName;
                        xlws.Cells[4 + cellindex, 3] = DateTime.Parse(drifts[i].Actions[j].StartTime).ToString("H:mm:ss");
                        xlws.Cells[4 + cellindex, 4] = DateTime.Parse(drifts[i].Actions[j].EndTime).ToString("H:mm:ss");
                        var start = DateTime.ParseExact(drifts[i].Actions[j].StartTime, "h:mm:ss tt", null);
                        var end = DateTime.ParseExact(drifts[i].Actions[j].EndTime, "h:mm:ss tt", null);
                        xlws.Cells[4 + cellindex, 5] = (end - start).ToString();
                        xlws.Cells[4 + cellindex, 6] = ((end - start) / drifts[i].Actions[j].EndWell).ToString();
                        if (j > 0)
                        {
                            var s1 = DateTime.ParseExact(drifts[i].Actions[j - 1].StartTime, "h:mm:ss tt", null);
                            var s2 = DateTime.ParseExact(drifts[i].Actions[j].StartTime, "h:mm:ss tt", null);
                            var l1 = DateTime.ParseExact(drifts[i].Actions[j - 1].EndTime, "h:mm:ss tt", null);
                            var l2 = DateTime.ParseExact(drifts[i].Actions[j].EndTime, "h:mm:ss tt", null);
                            xlws.Cells[4 + cellindex, 7] = (s2 - s1).ToString();
                            xlws.Cells[4 + cellindex, 8] = (l2 - l1).ToString();
                            var drifting = Math.Abs((l2 - l1).TotalSeconds - (s2 - s1).TotalSeconds);
                            xlws.Cells[4 + cellindex, 9] = TimeSpan.FromSeconds(drifting).ToString();
                        }
                        cellindex++;
                    }
                }

                xlApp.Visible = true;

                Marshal.ReleaseComObject(xlws);
                Marshal.ReleaseComObject(xlwb);
                Marshal.ReleaseComObject(xlApp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                xlwb.Close(false, misvalue, misvalue);
                xlApp.Quit();
            }
            finally
            {
                xlApp = null;
                xlwb = null;
                xlws = null;
            }
        }
    }
}

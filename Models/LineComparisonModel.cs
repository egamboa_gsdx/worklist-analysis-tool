﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace GSDWorklistLogAnalyzerTool.Models
{
    [AddINotifyPropertyChangedInterface]
    public class LineComparisonModel
    {
        public string OldText { get; set; }
        public string NewText { get; set; }
        public string OldTextHeader { get; set; }
        public string NewTextHeader { get; set; }
        public bool BtnIsEnabled { get; set; }
    }
}

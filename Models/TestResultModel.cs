﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace GSDWorklistLogAnalyzerTool.Models
{
    [AddINotifyPropertyChangedInterface]    
    public class TestResultModel
    {
        public string Name { get; set; }
        public ObservableCollection<ActionTimeModel> ActionTimes { get; set; }
        public TimeSpan IncubationTime { get; set; }
        public TimeSpan HeatActionTime { get; set; }
        public TimeSpan ShakerActionTime { get; set; }
    }
}

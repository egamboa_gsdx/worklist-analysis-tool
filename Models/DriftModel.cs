﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDWorklistLogAnalyzerTool.Models
{
    public class DriftModel
    {
        public DriftModel()
        {
            Actions = new List<ActionModel>();
        }
        public string TestName { get; set; }
        public List<ActionModel> Actions { get; set; }
    }
}

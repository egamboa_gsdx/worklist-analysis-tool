﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDWorklistLogAnalyzerTool.Models
{
    public class ActionTimeModel
    {
        public string TestRunName { get; set; }
        public string ActionName { get; set; }
        public TimeSpan ActionTime { get; set; }
    }
}

# **GSD TEST LAB WORKLIST ANALYSIS TOOL**
## READ ME
This tool has two main features. This Tool is used to caculate time durations of actions performed during a selected worklist and
also visually display any differences between two worklists that are comparable via text line comparison.

#### **NOTE**
The app currently freezes when doing line comparisons. Some investigation is needed but there are some visual queues that let the end
user know the app is processing.
﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GSDWorklistLogAnalyzerTool.Views;

namespace GSDWorklistLogAnalyzerTool.ViewModels
{
    public class MainWindowViewModel
    {
        private ITabViewModel _currentTab;
        private List<ITabViewModel> _tabs;
        public MainWindowViewModel()
        {
            _tabs = new List<ITabViewModel>()
            {
                SimpleIoc.Default.GetInstance<ResultTabViewModel>(),
                SimpleIoc.Default.GetInstance<LineComparisonViewModel>()
            };

            CurrentTab = _tabs[0];

            ChangeTabCommand = new RelayCommand<string>(ChangeTabExecute);
            AboutCommand = new RelayCommand(AboutExecute);
        }

        private void ChangeTabExecute(string name)
        {
            CurrentTab = Tabs.FirstOrDefault(vm => vm.Name == name);
        }

        private void AboutExecute()
        {
            var about = new AboutView();
            about.Show();
        }

        public List<ITabViewModel> Tabs
        {
            get
            {
                if (_tabs == null)
                {
                    _tabs = new List<ITabViewModel>();
                }
                return _tabs;
            }
            set
            {
                _tabs = value;
            }
        }
        public ITabViewModel CurrentTab
        {
            get { return _currentTab; }
            set
            {
                if (value == null)
                {
                    return;
                }
                if (_currentTab == null)
                {
                    _currentTab = _tabs[0];
                    _currentTab.IsSelected = true;
                    return;
                }

                foreach (var tab in _tabs)
                {
                    tab.IsSelected = false;
                }

                _currentTab = value;
                _currentTab.IsSelected = true;
            }
        }

        public ICommand ChangeTabCommand { get; set; }
        public ICommand AboutCommand { get; set; }
    }
}

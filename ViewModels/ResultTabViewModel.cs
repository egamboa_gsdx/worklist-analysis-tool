﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using GalaSoft.MvvmLight.Command;
using GSDWorklistLogAnalyzerTool.Models;
using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using PropertyChanged;
using Excel = Microsoft.Office.Interop.Excel;

namespace GSDWorklistLogAnalyzerTool.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class ResultTabViewModel : ITabViewModel
    {
        private string _fileName;
        private Random _rnd = new Random();
        private ResultTabModel _selectedTabDynamic;
        
        public ResultTabViewModel()
        {
            Name = "Worklist Action Times";
            Icon = PackIconKind.ChartBarStacked;
            TabItems = new ObservableCollection<ResultTabModel>();
            StatusLogs = new List<StatusLogModel>()
            {
                new StatusLogModel()
                {
                    Name = string.Empty,
                    WorklistName = string.Empty
                }
            };

            IsExportingExcel = false;
            AnalyzingFile = false;
            BtnIsEnabled = true;
            CompareBtnVisible = Visibility.Hidden;

            OpenFileCommand = new RelayCommand(OpenFileExecute);
            ExportExcelCommand = new RelayCommand<ResultTabModel>(ExportExcelExecute);
            CalculateDritfCommand = new RelayCommand<ResultTabModel>(CalculateDriftExecute);
            ExportExcelCompareCommand = new RelayCommand<ResultTabModel>(ExportCompareExecute);
            TabDeleteCommand = new RelayCommand<string>(TabDeleteExecute);
            CompareSelectionCommand = new RelayCommand(CompareSelectionExecute);    
        }

        private void OpenFileExecute()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt";

            if (openFileDialog.ShowDialog() == true)
            {
                _fileName = openFileDialog.FileName;
                RunAnalysis();
            }
        }
        private async void RunAnalysis()
        {
            var fileToRead = _fileName;
            ResultTabModel resulttab = new ResultTabModel();

            FileInfo fi = new FileInfo(fileToRead);
            if (!CheckFile(fi))
            {
                return;
            }
            

            try
            {
                await Task.Run(() =>
                {
                    AnalyzingFile = true;

                    var logAnalyzer = new LogAnalyzer(_fileName, resulttab);

                    var irecords = new ChartValues<double>();
                    var arecords = new List<ChartValues<double>>();
                    foreach (var test in resulttab.TestResults)
                    {
                        //arecords.Add(test.ActionTime.TotalSeconds);
                        foreach (var time in test.ActionTimes)
                        {
                            arecords.Add(new ChartValues<double>() { time.ActionTime.TotalSeconds });
                            resulttab.ActionTimes.Add(time);
                        }
                        irecords.Add(test.IncubationTime.TotalSeconds);
                    }

                    resulttab.IncubationResults = irecords;

                    int tabs = 1;
                    while (TabItems.Where(x => x.Header == string.Format("{0} RESULTS", resulttab.WorklistName)).Any())
                    {
                        string worklistnameonly = resulttab.WorklistName;
                        if (resulttab.WorklistName.Contains("("))
                        {
                            worklistnameonly = resulttab.WorklistName.Substring(0, resulttab.WorklistName.IndexOf(" ("));
                        }
                        resulttab.WorklistName = string.Format("{0} ({1})", worklistnameonly, tabs);
                        tabs++;
                    }
                    logAnalyzer.StatusLogs.WorklistName = resulttab.WorklistName;
                    StatusLogs.Add(logAnalyzer.StatusLogs);


                });
                ResultTabModel tab = (SelectedTabDynamic != null) ? SelectedTabDynamic as ResultTabModel : new ResultTabModel() { Header = string.Empty };
                if (tab != null && tab.Header != null)
                {
                    // tabDynamic.DataContext = null;

                    ResultTabModel newTab = this.AddTabItem(resulttab);

                    // tabDynamic.DataContext = TabItems;

                    SelectedTabDynamic = newTab;
                }
                else
                {
                    // TODO
                }
            } 
            catch (Exception ex)
            {
                MessageBox.Show($"Uh Oh! {ex.Message}.", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                AnalyzingFile = false;
            }
            
            
        }

        private bool CheckFile(FileInfo file)
        {
            if (file.Extension != ".txt")
            {
                MessageBox.Show("File cannot be used...make sure file is a Worklist Status Log.", "Invalid File", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            if (file.FullName.Contains("MLI"))
            {
                MessageBox.Show("Worklist Log is incompatible with timing analysis. Please select a worklist that does not evaluate MLI values", "Invalid Worklist Log",
                    MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            return true;
        }

        private ResultTabModel AddTabItem(ResultTabModel rtm)
        {
            int count = TabItems.Count;
            int tabs = 1;

            ResultTabModel tab = new ResultTabModel();
            tab.FileName = _fileName;
            while (TabItems.Where(x => x.Header == string.Format("{0} RESULTS", rtm.WorklistName)).Any())
            {
                string worklistnameonly = rtm.WorklistName;
                if (rtm.WorklistName.Contains("("))
                {
                    worklistnameonly = rtm.WorklistName.Substring(0, rtm.WorklistName.IndexOf(" ("));
                }
                rtm.WorklistName = string.Format("{0} ({1})", worklistnameonly, tabs);
                tabs++;
            }
            tab.Header = string.Format("{0} RESULTS", rtm.WorklistName);
            tab.Name = string.Format("{0}results", rtm.WorklistName);
            tab.WorklistName = rtm.WorklistName;
            tab.OperatorName = rtm.OperatorName;
            tab.NumberOfSamplesLoaded = rtm.NumberOfSamplesLoaded;
            tab.NumberOfTestsRun = rtm.NumberOfTestsRun;
            foreach (var testname in rtm.TestNames)
            {
                tab.TestNames.Add(testname);
            }
            foreach (var test in rtm.Tests)
            {
                tab.Tests.Add(test);
            }
            foreach (var statuslog in rtm.StatusLogs)
            {
                tab.StatusLogs.Add(statuslog);
            }
            foreach (var testresult in rtm.TestResults)
            {
                tab.TestResults.Add(testresult);
            }
            var newstatuslog = new StatusLogModel() { WorklistName = tab.WorklistName };
            tab.StatusLogs.Add(newstatuslog);

            tab.ActionResults = rtm.ActionResults;
            tab.IncubationResults = rtm.IncubationResults;

            tab.Wash1 = rtm.Wash1;
            tab.Wash2 = rtm.Wash2;
            tab.ApplicationName = rtm.ApplicationName;
            tab.ApplicationVersion = rtm.ApplicationVersion;
            tab.ComputerName = rtm.ComputerName;
            tab.InstrumentName = rtm.InstrumentName;
            tab.DateRan = rtm.DateRan;
            tab.TotalRuntime = rtm.TotalRuntime;
            tab.TabCount = TabItems.Count;
            tab.CalculateDriftEnabled = tab.ApplicationName.Contains("AIX") ? false : true;
            foreach (var actiontime in rtm.ActionTimes)
            {
                tab.ActionTimes.Add(actiontime);
            }
            // Bar Chart stuff
            tab.ActionTimesBarGraph = new ObservableCollection<ActionTimeModel>(tab.ActionTimes.OrderBy(x => x.ActionName));
            foreach (var actiontime in tab.ActionTimesBarGraph)
            {
                if (!(tab.SeriesCollection.Where(x => x.Title == actiontime.ActionName).Any()))
                {
                    tab.SeriesCollection.Add(new StackedColumnSeries()
                    {
                        Title = actiontime.ActionName,
                        Values = new ChartValues<double>(),
                        StackMode = StackMode.Percentage
                    });
                }

            }
            foreach (var series in tab.SeriesCollection)
            {
                foreach (var testresult in tab.TestResults)
                {
                    var val = testresult.ActionTimes.Where(x => x.ActionName == series.Title).Any() ?
                        testresult.ActionTimes.FirstOrDefault(x => x.ActionName == series.Title).ActionTime.TotalSeconds : 0;
                    series.Values.Add(val);
                }
            }

            tab.SeriesCollection.Add(new StackedColumnSeries()
            {
                Values = tab.IncubationResults,
                Fill = new SolidColorBrush() { Color = Colors.DarkViolet },
                Title = "Total Avg. Incubation Time",
                StackMode = StackMode.Percentage
            });

            TabItems.Add(tab);
            foreach(var tabitem in TabItems)
            {
                foreach (var statuslog in StatusLogs)
                {
                    if (!(tabitem.CompareList.Where(x => x.WorklistName == statuslog.WorklistName).Any()))
                    {
                        statuslog.Name = tab.Name;
                        tabitem.CompareList.Add(statuslog);
                    }    
                }
                tabitem.SelectedItem = tabitem.CompareList[0];
            }

            return tab;
        }

        private void TabDeleteExecute(string name)
        {
            var item = TabItems.Where(i => i.Name.Equals(name)).SingleOrDefault();

            ResultTabModel tab = item as ResultTabModel;

            if (tab != null)
            {
                if (MessageBox.Show(string.Format("Are you sure you want to remove the tab '{0}'?", tab.Header.ToString()), "Remove Tab", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    ResultTabModel selectedTab = SelectedTabDynamic as ResultTabModel;

                    
                    // tabDynamic.DataContext = null;
                    var deletestatlog = new StatusLogModel() { WorklistName = tab.WorklistName };
                    
                    //Reset each tab if comparing tab to be deleted
                    foreach (var tabitem in TabItems)
                    {
                        if (tabitem.SelectedItem.WorklistName == deletestatlog.WorklistName)
                            tabitem.SelectedItem = tabitem.CompareList[0];

                        tabitem.CompareList.Remove(deletestatlog);
                        tabitem.StatusLogs.Remove(deletestatlog);

                        CompareSelectionCommand.Execute(tabitem.SelectedItem);
                    }
                    tab.StatusLogs.Remove(deletestatlog);
                    StatusLogs.Remove(deletestatlog);
                    TabItems.Remove(tab);


                    // tabDynamic.DataContext = _tabItems;

                    if (selectedTab == null || selectedTab.Equals(tab))
                    {
                        if (TabItems.Any()) selectedTab = TabItems[TabItems.Count - 1];
                        else selectedTab = null;
                    }
                    SelectedTabDynamic = selectedTab;
                    
                }
            }
        }
        private void CompareSelectionExecute()
        {
            ResultTabModel rt = SelectedTabDynamic;
            if (rt != null)
            {
                if (rt.SelectedItem == null)
                {
                    rt.SelectedItem = rt.CompareList[0];
                }

                if (rt.SelectedItem is StatusLogModel)
                {
                    string tabName = rt.Name;
                    string comparewl = rt.SelectedItem.WorklistName;
                    //CompareName = comparewl;
                    var item = TabItems.Where(i => i.Name.Equals(tabName)).SingleOrDefault();
                    var compareitem = TabItems.Where(i => i.WorklistName.Equals(comparewl)).SingleOrDefault();

                    ResultTabModel tab = item as ResultTabModel;
                    ResultTabModel comparetab = compareitem as ResultTabModel;
                    if (string.IsNullOrEmpty(comparewl))
                    {
                        tab.CompareCollection = new SeriesCollection();
                        tab.CompareTestLabels = new ObservableCollection<string>();
                        tab.ExportEnabled = tab.CanExport();
                        tab.CompareBtnVisible = Visibility.Hidden;
                        tab.DetailTextLeft = string.Empty;
                        tab.DetailTextRight = string.Empty;
                        tab.DetailTextMid = string.Empty;
                        tab.DetailHeader1 = string.Empty;
                        tab.DetailHeader2 = string.Empty;
                        return;
                    }

                    if (tab != null)
                    {
                        if (tab.TestNames.SequenceEqual(comparetab.TestNames))
                        {
                            tab.CompareCollection = new SeriesCollection();
                            tab.CompareTestLabels = new ObservableCollection<string>();

                            foreach (var actiontime in comparetab.ActionTimesBarGraph)
                            {
                                if (!(tab.CompareCollection.Where(x => x.Title == actiontime.ActionName).Any()))
                                {
                                    Brush result = Brushes.Transparent;
                                    Type brushestype = typeof(Brushes);

                                    PropertyInfo[] properties = brushestype.GetProperties();

                                    int random = _rnd.Next(properties.Length);
                                    result = (Brush)properties[random].GetValue(null, null);

                                    tab.CompareCollection.Add(new StackedColumnSeries()
                                    {
                                        Title = actiontime.ActionName,
                                        Values = new ChartValues<double>(),
                                        StackMode = StackMode.Percentage,
                                        Fill = result
                                    });
                                }

                            }
                            foreach (var series in tab.CompareCollection)
                            {
                                foreach (var testresult in comparetab.TestResults)
                                {
                                    var val = testresult.ActionTimes.Where(x => x.ActionName == series.Title).Any() ?
                                        testresult.ActionTimes.First(x => x.ActionName == series.Title).ActionTime.TotalSeconds : 0;
                                    series.Values.Add(val);
                                }
                            }

                            tab.CompareCollection.Add(new StackedColumnSeries()
                            {
                                Values = comparetab.IncubationResults,
                                Title = "Total Avg. Incubation Time",
                                StackMode = StackMode.Percentage
                            });
                            foreach (var label in comparetab.TestNames)
                            {
                                tab.CompareTestLabels.Add(label);
                            }
                            tab.ExportEnabled = tab.CanExport();
                            tab.CompareBtnVisible = Visibility.Visible;
                            ShowDetail(tab, comparetab);
                        }
                        else
                        {
                            MessageBox.Show("The Worklists do not have the same Test Run...Please select worklists with similiar tests",
                                "Invalid Test Runs",
                                MessageBoxButton.OK,
                                MessageBoxImage.Warning);
                            tab.SelectedItem = tab.CompareList[0];
                            tab.CompareBtnVisible = Visibility.Hidden;
                        }
                    }
                }
            }
        }

        private async void ShowDetail(ResultTabModel tab, ResultTabModel comparetab)
        {
            await Task.Run(() =>
            {
                BtnIsEnabled = false;
                IsExportingExcel = true;

                if (tab != null)
                {
                    tab.DetailHeader1 = tab.WorklistName;
                    tab.DetailHeader2 = comparetab.WorklistName;
                    var currenttest1 = tab.TestNames.First(x => x.Contains(tab.ActionTimes[0].TestRunName));
                    var currenttest2 = comparetab.TestNames.First(x => x.Contains(comparetab.ActionTimes[0].TestRunName));
                    tab.DetailTextLeft = "\nAction Time Duration (dd.hh:mm:ss)\n";
                    tab.DetailTextRight = "\nAction Time Duration (dd.hh:mm:ss)\n";
                    tab.DetailTextMid = currenttest1 + "\nAction Name(s)\n";

                    // Get all actions from both tabs
                    var actionlist = new List<Tuple<string, string>>();

                    foreach (var action in tab.ActionTimes)
                    {
                        actionlist.Add(new Tuple<string, string>(action.TestRunName, action.ActionName));
                    }

                    foreach (var action in comparetab.ActionTimes)
                    {
                        if (!actionlist.Any(x => x.Item1.Equals(action.TestRunName) && x.Item2.Equals(action.ActionName)))
                        {
                            actionlist.Add(new Tuple<string, string>(action.TestRunName, action.ActionName));
                        }
                    }

                    actionlist = new List<Tuple<string, string>>(actionlist.OrderBy(x => x.Item1).ThenBy(x => x.Item2));

                    

                    for (int i = 0; i < actionlist.Count; i++)
                    {
                        if (!(currenttest1.Contains(actionlist[i].Item1)))
                        {
                            var s1 = tab.TestResults.First(x => x.Name.Equals(actionlist[i - 1].Item1));
                            var s2 = comparetab.TestResults.First(x => x.Name.Equals(actionlist[i - 1].Item1));
                            tab.DetailTextLeft += s1.IncubationTime + "\n";
                            tab.DetailTextMid += "--------------------------------------------" + "Total Avg. Inucbation" + "--------------------------------------------\n";
                            tab.DetailTextRight += s2.IncubationTime + "\n";
                            currenttest1 = tab.TestNames.First(x => x.Contains(actionlist[i].Item1));
                            tab.DetailTextMid += "\n" + currenttest1 + "\nAction Name(s)\n";
                            tab.DetailTextLeft += "\n\nAction Time Duration (dd.hh:mm:ss)\n";
                            tab.DetailTextRight += "\n\nAction Time Duration (dd.hh:mm:ss)\n";
                        }
                        else
                        {
                            tab.DetailTextLeft += tab.ActionTimes.Any(x => x.ActionName.Equals(actionlist[i].Item2) && x.TestRunName.Equals(actionlist[i].Item1)) ? 
                                tab.ActionTimes.First(x => x.ActionName.Equals(actionlist[i].Item2) && x.TestRunName.Equals(actionlist[i].Item1)).ActionTime + "\n"
                                : "00:00:00\n";
                            tab.DetailTextMid += "--------------------------------------------" + actionlist[i].Item2 + "--------------------------------------------\n";
                            tab.DetailTextRight += comparetab.ActionTimes.Any(x => x.ActionName.Equals(actionlist[i].Item2) && x.TestRunName.Equals(actionlist[i].Item1)) ? 
                                comparetab.ActionTimes.First(x => x.ActionName.Equals(actionlist[i].Item2) && x.TestRunName.Equals(actionlist[i].Item1)).ActionTime + "\n"
                                : "00:00:00\n";
                        }
                    }

                    var last1 = tab.TestResults.Last();
                    var last2 = comparetab.TestResults.Last();
                    tab.DetailTextLeft += last1.IncubationTime + "\n";
                    tab.DetailTextMid += "--------------------------------------------" + "Total Avg. Inucbation" + "--------------------------------------------\n";
                    tab.DetailTextRight += last2.IncubationTime + "\n";

                    tab.DetailTextLeft += "--------\n";
                    tab.DetailTextMid += "------------------------------------------------------------------------------------------------------------------------\n";
                    tab.DetailTextRight += "--------\n";

                    tab.DetailTextLeft += "\n" + tab.TotalRuntime + "\n";
                    tab.DetailTextMid += "\nTotal Worklist Runtime" + "\n";
                    tab.DetailTextRight += "\n" + comparetab.TotalRuntime + "\n";

                }
            });

            Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.ApplicationIdle,
                new Action(() =>
                {
                    BtnIsEnabled = true;
                    IsExportingExcel = false;
                })).Wait();

        }

        private async void ExportExcelExecute(ResultTabModel rt)
        {
            await Task.Run(() =>
            {
                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                Excel.Workbook xlworkbook = null;
                Excel.Worksheet xlworksheet = null;
                Excel.Range er = null;
                object misValue = System.Reflection.Missing.Value;

                IsExportingExcel = true;
                rt.CalculateDriftEnabled = false;
                CheckBtnEnabled();
                if (xlApp == null)
                {
                    MessageBox.Show("Excel is not properly installed on this computer! Sucka!", "Excel Not Installed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                string tabName = rt.Name;

                var item = rt;

                ResultTabModel tab = item as ResultTabModel;
                if (tab != null)
                {

                    try
                    {
                        xlworkbook = xlApp.Workbooks.Add(misValue);
                        xlworksheet = (Excel.Worksheet)xlworkbook.Worksheets.get_Item(1);
                        xlworksheet.Name = tab.WorklistName.Substring(0, 30);

                        // add data
                        // Worklsit Information
                        // 'Headers'
                        xlworksheet.Cells[1, 1] = "Worklist Name:";
                        xlworksheet.Cells[2, 1] = "Operator:";
                        xlworksheet.Cells[3, 1] = "Number of Samples Loaded:";
                        xlworksheet.Cells[4, 1] = "Number of Test(s) Ran:";
                        xlworksheet.Cells[5, 1] = "Wash Bottle 1:";
                        xlworksheet.Cells[6, 1] = "Wash Bottle 2:";
                        er = xlworksheet.get_Range("A1:A7", misValue);
                        er.Font.Bold = true;

                        xlworksheet.Cells[1, 4] = "Application Name:";
                        xlworksheet.Cells[2, 4] = "Application Version:";
                        xlworksheet.Cells[3, 4] = "Computer Name:";
                        xlworksheet.Cells[4, 4] = "Instrument Name:";
                        xlworksheet.Cells[5, 4] = "Date Ran:";
                        xlworksheet.Cells[6, 4] = "Total Runtime (dd.hh:mm:ss):";
                        er = xlworksheet.get_Range("D1:D7", misValue);
                        er.Font.Bold = true;

                        // Worklist Data
                        xlworksheet.Range[xlworksheet.Cells[1, 2], xlworksheet.Cells[1, 3]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[2, 2], xlworksheet.Cells[2, 3]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[3, 2], xlworksheet.Cells[3, 3]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[4, 2], xlworksheet.Cells[4, 3]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[5, 2], xlworksheet.Cells[5, 3]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[6, 2], xlworksheet.Cells[6, 3]].Merge();

                        xlworksheet.Range[xlworksheet.Cells[1, 5], xlworksheet.Cells[1, 6]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[2, 5], xlworksheet.Cells[2, 6]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[3, 5], xlworksheet.Cells[3, 6]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[4, 5], xlworksheet.Cells[4, 6]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[5, 5], xlworksheet.Cells[5, 6]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[6, 5], xlworksheet.Cells[6, 6]].Merge();

                        er = xlworksheet.get_Range("B3", misValue);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                        er = xlworksheet.get_Range("B4", misValue);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
                        er = xlworksheet.get_Range("E5", misValue);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                        xlworksheet.Cells[1, 2] = tab.WorklistName;
                        xlworksheet.Cells[2, 2] = tab.OperatorName;
                        xlworksheet.Cells[3, 2] = tab.NumberOfSamplesLoaded.ToString();
                        xlworksheet.Cells[4, 2] = tab.NumberOfTestsRun.ToString();
                        xlworksheet.Cells[5, 2] = tab.Wash1;
                        xlworksheet.Cells[6, 2] = tab.Wash2;

                        xlworksheet.Cells[1, 5] = tab.ApplicationName;
                        xlworksheet.Cells[2, 5] = tab.ApplicationVersion;
                        xlworksheet.Cells[3, 5] = tab.ComputerName;
                        xlworksheet.Cells[4, 5] = tab.InstrumentName;
                        xlworksheet.Cells[5, 5] = tab.DateRan;
                        xlworksheet.Cells[6, 5] = tab.TotalRuntime.ToString("c");
                        er = xlworksheet.get_Range("E6", misValue);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                        // Test data
                        xlworksheet.Cells[8, 1] = "Test Name";
                        xlworksheet.Cells[8, 2] = "Total Avg. Incubation Time (dd.hh:mm:ss)";
                        xlworksheet.Cells[8, 3] = "Action";
                        xlworksheet.Cells[8, 4] = "Action Time (dd.hh:mm:ss)";

                        er = xlworksheet.get_Range("8:8", misValue);
                        er.Font.Bold = true;

                        er = xlworksheet.get_Range("A:E", misValue);
                        er.EntireColumn.ColumnWidth = 40;



                        var currenttest = tab.TestNames.Where(x => x.Contains(tab.ActionTimes[0].TestRunName)).FirstOrDefault();
                        var incubationindex = 0;
                        xlworksheet.Cells[9, 1] = currenttest;
                        xlworksheet.Cells[9, 2] = TimeSpan.FromSeconds(tab.IncubationResults[incubationindex]).ToString("c");

                        for (int i = 0; i < tab.ActionTimes.Count; i++)
                        {
                            if (!(currenttest.Contains(tab.ActionTimes[i].TestRunName)))
                            {
                                currenttest = tab.TestNames.Where(x => x.Contains(tab.ActionTimes[i].TestRunName)).FirstOrDefault();
                                xlworksheet.Cells[9 + i, 1] = currenttest;
                                xlworksheet.Cells[9 + i, 2] = TimeSpan.FromSeconds(tab.IncubationResults[++incubationindex]).ToString("c");
                            }
                            xlworksheet.Cells[9 + i, 3] = tab.ActionTimes[i].ActionName;
                            xlworksheet.Cells[9 + i, 4] = tab.ActionTimes[i].ActionTime.ToString("c");
                        }
                        Excel.Range last = xlworksheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, misValue);
                        er = xlworksheet.get_Range("B9", last);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        er = xlworksheet.get_Range("C:C", misValue);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                        xlApp.Visible = true;

                        Marshal.ReleaseComObject(xlworksheet);
                        Marshal.ReleaseComObject(xlworkbook);
                        Marshal.ReleaseComObject(xlApp);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        xlworkbook.Close(false, misValue, misValue);
                        xlApp.Quit();
                    }
                    finally
                    {
                        xlApp = null;
                        xlworkbook = null;
                        xlworksheet = null;
                    }
                }
            });

            IsExportingExcel = false;
            rt.CalculateDriftEnabled = rt.ApplicationName.Contains("AIX") ? false : true;
            CheckBtnEnabled();
        }

        private async void ExportCompareExecute(ResultTabModel rt)
        {
            await Task.Run(() =>
            {
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlworkbook = null;
                Excel.Worksheet xlworksheet = null;
                Excel.Range er = null;
                object misValue = System.Reflection.Missing.Value;

                IsExportingExcel = true;
                rt.CalculateDriftEnabled = false;
                CheckBtnEnabled();
                if (xlApp == null)
                {
                    MessageBox.Show("Excel is not properly installed on this computer! Sucka!", "Excel Not Installed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (rt != null)
                {
                    var compareitem = TabItems.FirstOrDefault(i => i.WorklistName.Equals(rt.SelectedItem.WorklistName));

                    var actionlist = new List<Tuple<string, string>>();

                    foreach (var item in rt.ActionTimes)
                    {
                        actionlist.Add(new Tuple<string, string>(item.TestRunName, item.ActionName));
                    }

                    foreach (var item in compareitem.ActionTimes)
                    {
                        if (!actionlist.Any(x => x.Item1.Equals(item.TestRunName) && x.Item2.Equals(item.ActionName)))
                        {
                            actionlist.Add(new Tuple<string, string>(item.TestRunName, item.ActionName));
                        }
                    }

                    actionlist = new List<Tuple<string, string>>(actionlist.OrderBy(x => x.Item1).ThenBy(x => x.Item2));
                    try
                    {
                        xlworkbook = xlApp.Workbooks.Add(misValue);
                        xlworksheet = (Excel.Worksheet)xlworkbook.Worksheets.get_Item(1);

                        er = xlworksheet.get_Range("A:A", misValue);
                        er.EntireColumn.ColumnWidth = 20;

                        er = xlworksheet.get_Range("B:D", misValue);
                        er.EntireColumn.ColumnWidth = 45;
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;

                        xlworksheet.Range[xlworksheet.Cells[1, 2], xlworksheet.Cells[1, 4]].Merge();
                        xlworksheet.Range[xlworksheet.Cells[2, 2], xlworksheet.Cells[2, 4]].Merge();

                        xlworksheet.Cells[1, 2] = rt.WorklistName;
                        xlworksheet.Cells[2, 2] = rt.SelectedItem.WorklistName;

                        er = xlworksheet.get_Range("B1", misValue);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

                        er = xlworksheet.get_Range("B2", misValue);
                        er.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;


                        xlworksheet.Cells[1, 1] = "Worklist 1:";
                        xlworksheet.Cells[2, 1] = "Worklist 2:";

                        var currenttest = rt.TestNames.Where(x => x.Contains(rt.ActionTimes[0].TestRunName)).FirstOrDefault();

                        xlworksheet.Cells[4, 3] = currenttest;
                        xlworksheet.Cells[5, 2] = "Worklist 1 Action Time Duration (dd.hh:mm:ss)";
                        xlworksheet.Cells[5, 3] = "Action Name";
                        xlworksheet.Cells[5, 4] = "Worklist 2 Action Time Duration (dd.hh:mm:ss)";
                        er = xlworksheet.get_Range("C4", misValue);
                        er.Font.Bold = true;

                        er = xlworksheet.get_Range("B5:D5", misValue);
                        er.Font.Bold = true;

                        int rowindex = 6;

                        for (int i = 0; i < actionlist.Count; i++)
                        {
                            if (!(currenttest.Contains(actionlist[i].Item1)))
                            {
                                var s1 = rt.TestResults.Where(x => x.Name.Equals(actionlist[i - 1].Item1)).FirstOrDefault();
                                var s2 = compareitem.TestResults.Where(x => x.Name.Equals(actionlist[i - 1].Item1)).FirstOrDefault();
                                xlworksheet.Cells[rowindex, 2] = s1.IncubationTime.ToString("c");
                                xlworksheet.Cells[rowindex, 3] = "Total Avg. Incubation";
                                xlworksheet.Cells[rowindex, 4] = s2.IncubationTime.ToString("c");
                                rowindex++;

                                currenttest = rt.TestNames.Where(x => x.Contains(actionlist[i].Item1)).FirstOrDefault();

                                rowindex++;
                                xlworksheet.Cells[rowindex, 3] = currenttest;
                                string s = string.Format("C{0}", rowindex);
                                er = xlworksheet.get_Range(s, misValue);
                                er.Font.Bold = true;
                                rowindex++;

                                xlworksheet.Cells[rowindex, 2] = "Worklist 1 Action Time Duration (dd.hh:mm:ss)";
                                xlworksheet.Cells[rowindex, 3] = "Action Name";
                                xlworksheet.Cells[rowindex, 4] = "Worklist 2 Action Time Duration (dd.hh:mm:ss)";
                                er = xlworksheet.get_Range(string.Format("B{0}:D{0}", rowindex), misValue);
                                er.Font.Bold = true;
                                rowindex++;

                            }

                            xlworksheet.Cells[rowindex, 2] = rt.ActionTimes.Any(x => x.ActionName.Equals(actionlist[i].Item2) && x.TestRunName.Equals(actionlist[i].Item1)) ?
                                rt.ActionTimes.First(x => x.TestRunName.Equals(actionlist[i].Item1) && x.ActionName.Equals(actionlist[i].Item2)).ActionTime.ToString("c") : TimeSpan.Zero.ToString("c");
                            xlworksheet.Cells[rowindex, 3] = actionlist[i].Item2;
                            xlworksheet.Cells[rowindex, 4] = compareitem.ActionTimes.Any(x => x.ActionName.Equals(actionlist[i].Item2) && x.TestRunName.Equals(actionlist[i].Item1)) ?
                                compareitem.ActionTimes.First(x => x.ActionName.Equals(actionlist[i].Item2) && x.TestRunName.Equals(actionlist[i].Item1)).ActionTime.ToString("c") : TimeSpan.Zero.ToString("c");
                            rowindex++;
                        }

                        var last1 = rt.TestResults.Last();
                        var last2 = compareitem.TestResults.Last();
                        xlworksheet.Cells[rowindex, 2] = last1.IncubationTime.ToString("c");
                        xlworksheet.Cells[rowindex, 3] = "Total Avg. Incubation";
                        xlworksheet.Cells[rowindex, 4] = last2.IncubationTime.ToString("c");

                        rowindex += 2;
                        xlworksheet.Cells[rowindex, 2] = rt.TotalRuntime.ToString("c");
                        xlworksheet.Cells[rowindex, 3] = "Total Worklist Runtime";
                        xlworksheet.Cells[rowindex, 4] = compareitem.TotalRuntime.ToString("c");


                        xlApp.Visible = true;

                        Marshal.ReleaseComObject(xlworksheet);
                        Marshal.ReleaseComObject(xlworkbook);
                        Marshal.ReleaseComObject(xlApp);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        xlworkbook.Close(false, misValue, misValue);
                        xlApp.Quit();
                    }
                    finally
                    {
                        xlApp = null;
                        xlworkbook = null;
                        xlworksheet = null;
                    }
                }
            });

            IsExportingExcel = false;
            rt.CalculateDriftEnabled = rt.ApplicationName.Contains("AIX") ? false : true;
            CheckBtnEnabled();
        }
        private async void CalculateDriftExecute( ResultTabModel rt)
        {
            await Task.Run(() =>
            {
                IsExportingExcel = true;
                CheckBtnEnabled();
                rt.CalculateDriftEnabled = false;
                var item = rt;

                ResultTabModel tab = item as ResultTabModel;

                if (rt != null)
                {
                    CalculateDriftModel cdm = new CalculateDriftModel(rt.FileName);
                    cdm.CalculateDrift();
                }
            });

            IsExportingExcel = false;
            rt.CalculateDriftEnabled = rt.ApplicationName.Contains("AIX") ? false : true;
            CheckBtnEnabled();
        }

        private void CheckBtnEnabled()
        {
            BtnIsEnabled = !IsExportingExcel;
        }

        public ObservableCollection<ResultTabModel> TabItems { get; set; }
        public ResultTabModel SelectedTabDynamic
        {
            get
            {
                return _selectedTabDynamic;
            }
            set
            {
                if (_selectedTabDynamic == value) return;
                _selectedTabDynamic = value;
            }
        }
        public List<StatusLogModel> StatusLogs { get; set; }
        public bool IsExportingExcel { get; set; }
        public bool BtnIsEnabled { get; set; }
        public bool AnalyzingFile { get; set; }
        public Visibility CompareBtnVisible { get; set; }

        private StatusLogModel _selectedTabItem;
        public StatusLogModel SelectedTabItem
        {
            get { return _selectedTabItem; }
            set
            {
                if (_selectedTabItem == value) return;
                _selectedTabItem = value;
            }
        }
        public ICommand OpenFileCommand { get; set; }
        public ICommand ExportExcelCommand { get; set; }
        public ICommand ExportExcelCompareCommand { get; set; }
        public RelayCommand<ResultTabModel> CalculateDritfCommand { get; set; }
        public ICommand TabDeleteCommand { get; set; }
        public ICommand CompareSelectionCommand { get; set; }
        public ICommand DataToolTipCommand { get; set; }
        public bool IsSelected { get; set; }
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
    }
}

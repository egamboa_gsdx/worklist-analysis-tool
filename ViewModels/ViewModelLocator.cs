﻿using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSDWorklistLogAnalyzerTool.ViewModels;

namespace GSDWorklistLogAnalyzerTool
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            SimpleIoc.Default.Register<MainWindowViewModel>();
            SimpleIoc.Default.Register<ResultTabViewModel>();
            SimpleIoc.Default.Register<LineComparisonViewModel>();
        }

        public MainWindowViewModel MainVM => SimpleIoc.Default.GetInstance<MainWindowViewModel>();
        public LineComparisonViewModel LineComparisonVM => SimpleIoc.Default.GetInstance<LineComparisonViewModel>();
        public ResultTabViewModel ResultTabVM => SimpleIoc.Default.GetInstance<ResultTabViewModel>();
    }
}

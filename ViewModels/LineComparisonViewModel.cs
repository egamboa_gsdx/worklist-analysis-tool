﻿using DiffPlex;
using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;
using GalaSoft.MvvmLight.Command;
using GSDWorklistLogAnalyzerTool.Models;
using GSDWorklistLogAnalyzerTool.Views;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using PropertyChanged;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace GSDWorklistLogAnalyzerTool.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class LineComparisonViewModel : ITabViewModel
    {
        private string _filename1;
        private string _filename2;
        private static object _syncLock = new object();
        private BackgroundWorker _worker = new BackgroundWorker();
        private Differ _differ;
        public LineComparisonViewModel()
        {
            Name = "Line Comparison";
            Icon = PackIconKind.TextBoxSearchOutline;
            
            OpenFileCommand = new RelayCommand(OpenFileExecute);
            ClearCommand = new RelayCommand(ClearDiffViewExecute);
            GoToCommand = new RelayCommand(GoToLineExecute);
            ChangeIndexCommand = new RelayCommand<string>(ChangeIndexExecute);
            LineDifference = new ObservableCollection<LineDiffModel>();
            LineIndex = 0;
            ProgressPercent = 0;
            LoadingText = "Hidden";

            _worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
            _worker.ProgressChanged += Worker_ProgressChanged;
            _worker.RunWorkerCompleted += Worker_RunCompleted;
            _worker.WorkerReportsProgress = true;
            BindingOperations.EnableCollectionSynchronization(LineDifference, _syncLock);
        }
        public ICommand OpenFileCommand { get; set; }
        public ICommand ClearCommand { get; set; }
        public ICommand GoToCommand { get; set; }
        public ICommand ChangeIndexCommand { get; set; }
        public string OldText { get; set; }
        public string NewText { get; set; }
        public string OldTextHeader { get; set; }
        public string NewTextHeader { get; set; }
        public ObservableCollection<LineDiffModel> LineDifference { get; set; }
        public int LineIndex { get; set; }
        public int ProgressPercent { get; set; }
        public string LoadingText { get; set; }
        public bool IsSelected { get; set; }
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
        public Effect Effect { get; set; }
        public Differ Differ
        {
            get { return _differ; }
            set
            {
                if (value == null)
                {
                    return;
                }
                if (_differ == null)
                {
                    _differ = new Differ();
                    return;
                } 
                _differ = value;
            } 
        }

        private void OpenFileExecute()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                _filename1 = openFileDialog.FileName;

                MessageBox.Show("Please select file to compare.", "Compare File", MessageBoxButton.OK, MessageBoxImage.Information);
                OpenFileDialog openCompareFileDialog = new OpenFileDialog();
                openCompareFileDialog.Filter = "txt files (*.txt)|*.txt";

                if (openCompareFileDialog.ShowDialog() == true)
                {
                    _filename2 = openCompareFileDialog.FileName;
                    _worker.RunWorkerAsync();
                }
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ProgressPercent = e.ProgressPercentage;
        }

        private void Worker_RunCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ProgressPercent = 0;
            LoadingText = "Hidden";
            Effect = null;
        }
        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!string.IsNullOrEmpty(_filename1) && !string.IsNullOrEmpty(_filename2))
            {
                
                try
                {
                    string file1text = string.Empty;
                    string file2text = string.Empty;

                    _worker.ReportProgress(0);
                    Effect = new BlurEffect();

                    _worker.ReportProgress(10);
                    LoadingText = "Visible";

                    StreamReader sr1 = new StreamReader(_filename1);
                    StreamReader sr2 = new StreamReader(_filename2);

                    string line1;
                    string line2;
                    while ((line1 = sr1.ReadLine()) != null)
                    {
                        file1text = file1text + (line1.Substring(line1.IndexOf(";") + 1)) + "\n";
                    }

                    while ((line2 = sr2.ReadLine()) != null)
                    {
                        file2text = file2text + (line2.Substring(line2.IndexOf(";") + 1)) + "\n";
                    }
                    _worker.ReportProgress(25);

                    //Dispatcher.Invoke(new Action(GetLineDiffer(file1text, file2text)), DispatcherPriority.ContextIdle, null);

                    SideBySideDiffModel result = null;

                    _differ = new Differ();
                    var builder = new SideBySideDiffBuilder(_differ);

                    result = builder.BuildDiffModel(file1text, file2text);

                    int i = 0;

                    foreach (var line in result.NewText.Lines)
                    {
                        i++;
                        if (line.Type == ChangeType.Inserted)
                        {
                            LineDifference.Add(new LineDiffModel()
                            {
                                Index = i,
                                LineDiff = string.Format("Line {0} has been inserted.", i)
                            });
                        }
                        else if (line.Type == ChangeType.Deleted)
                        {
                            LineDifference.Add(new LineDiffModel()
                            {
                                Index = i,
                                LineDiff = string.Format("Line {0} has been deleted.", i)
                            });
                        }
                        else if (line.Type == ChangeType.Modified)
                        {
                            LineDifference.Add(new LineDiffModel()
                            {
                                Index = i,
                                LineDiff = string.Format("Line {0} has been modified.", i)
                            });
                        }
                        else if (line.Type == ChangeType.Imaginary)
                        {
                            LineDifference.Add(new LineDiffModel()
                            {
                                Index = i,
                                LineDiff = string.Format("Line {0} is imaginary.", i)
                            });
                        }
                    }

                    OldTextHeader = _filename1.Substring(_filename1.LastIndexOf(@"\") + 1);
                    NewTextHeader = _filename2.Substring(_filename2.LastIndexOf(@"\") + 1);

                    
                    _worker.ReportProgress(75);

                    OldText = file1text;
                    NewText = file2text;

                    Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.ApplicationIdle,
                        new Action(() =>
                        {
                            
                        })).Wait();
                }
                catch
                {

                }
            }
        }

        private async void ClearDiffViewExecute()
        {
            LoadingText = "Visible";
            await Task.Run(() =>
            {

                OldText = string.Empty;
                OldTextHeader = string.Empty;

                NewText = string.Empty;
                NewTextHeader = string.Empty;

                LineDifference.Clear();

                Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.ApplicationIdle,
                new Action(() =>
                {
                    LoadingText = "Hidden";

                })).Wait();
            });
            LoadingText = "Hidden";
        }
        private async void GoToLineExecute()
        {
            await Task.Run(() =>
            {
                LineIndex = 100;
            });
        }
        private void ChangeIndexExecute(string index)
        {
            LineIndex = Convert.ToInt32(index);
        }
    }
}

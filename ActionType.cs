﻿namespace GSDWorklistLogAnalyzerTool.Enums
{
    public enum ActionType
    {
        FluidTransfer,
        MultiShot,
        WashWell,
        ReadWell,
        Other,
        NoWashDispense,
        Shaker,
        Heat
    }
}

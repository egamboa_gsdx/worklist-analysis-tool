﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GSDWorklistLogAnalyzerTool.Views
{
    /// <summary>
    /// Interaction logic for AboutView.xaml
    /// </summary>
    public partial class AboutView : Window
    {
        public AboutView()
        {
            InitializeComponent();

            if (File.Exists(System.IO.Path.GetFullPath(".") + @"\README.md"))
            {
                MarkdownView.Markdown = File.ReadAllText(System.IO.Path.GetFullPath(".") + @"\README.md");
            }
            else
            {
                MarkdownView.Markdown = "#### No Information Available...";
            }
        }
    }
}

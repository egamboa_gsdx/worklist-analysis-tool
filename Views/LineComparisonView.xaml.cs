﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PropertyChanged;
using DiffPlex.DiffBuilder;
using GalaSoft.MvvmLight.Command;
using GSDWorklistLogAnalyzerTool.ViewModels;

namespace GSDWorklistLogAnalyzerTool.Views
{
    /// <summary>
    /// Interaction logic for LineComparisonView.xaml
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public partial class LineComparisonView : UserControl
    {
        public LineComparisonView()
        {
            InitializeComponent();
            ViewIndex = 1;
            BackToTopbtn.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var index = (sender as Button).CommandParameter.ToString();
            DiffView.GoTo(Convert.ToInt32(index), true);
            ViewIndex = Convert.ToInt32(index);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ContentPresenter cp = e.OriginalSource as ContentPresenter;
            int index;
            ListBoxItem item = (ListBoxItem)(LineDifflb.ItemContainerGenerator.ContainerFromItem(LineDifflb.Items.CurrentItem));

            
            if (item != null)
            {
                ContentPresenter contentPresenter = FindVisualChild<ContentPresenter>(item);

                DataTemplate dataTemplate = contentPresenter.ContentTemplate;
                Button button = (Button)dataTemplate.FindName("linediffbtn", contentPresenter);
                index = Convert.ToInt32(button.CommandParameter.ToString());
                DiffView.GoTo(index, true);
                ViewIndex = index;
            }   
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                {
                    return (childItem)child;
                }
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DiffView.GoTo(0, true);
            LineDifflb.UnselectAll();
            BackToTopbtn.Visibility = Visibility.Hidden;
        }

        public bool BackToTopEnabled { get; set; }
        public int ViewIndex { get; set; }

        private void DiffView_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            var pos = e.OriginalSource as ScrollViewer;

            if (pos != null)
            {
                if (pos.ScrollableHeight > 0 && pos.VerticalOffset > 0)
                    BackToTopbtn.Visibility = Visibility.Visible;
                else
                    BackToTopbtn.Visibility = Visibility.Hidden;

            }
        }
    }
}

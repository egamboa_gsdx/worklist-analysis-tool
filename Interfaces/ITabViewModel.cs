﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSDWorklistLogAnalyzerTool
{
    public interface ITabViewModel
    {
        public bool IsSelected { get; set; }
        public string Name { get; set; }
        public PackIconKind Icon { get; set; }
    }
}
